terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}
// Configure the Yandex.Cloud provider_______________
provider "yandex" {
  token     = "${var.token}"
  folder_id = "${var.folder_id}"
  cloud_id  = "${var.cloud_id}"
  zone      = "ru-central1-a"
}

data "yandex_compute_image" "base_image" {
  family = "ubuntu-2004-lts"
}
data "yandex_vpc_subnet" "default" {
  name = "default-ru-central1-a"
}

// Create a new instances____________________________
resource "yandex_compute_instance" "instance" {
  count       = "${var.cluster_size}"
  name        = "${element(var.instance_name, count.index)}-${element(var.zones, count.index)}"
  hostname    = "${element(var.instance_name, count.index)}-${count.index}"
  description = "${element(var.instance_name, count.index)}-${count.index} of my cluster"
  zone        = "${element(var.zones, count.index)}"

  resources {
    cores         = "${var.cores}"
    memory        = "${var.memory}"
    core_fraction = "${var.core_fraction}"
    
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.base_image.id
      size = 20
    }
    
  }

  network_interface {
    subnet_id = "${element(local.subnet_ids.0,count.index)}"
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

}

resource "yandex_vpc_network" "network" {
  name = "yc-auto-subnet"
}

resource "yandex_vpc_subnet" "subnet" {
  count          = "${var.cluster_size > length(var.zones) ? length(var.zones)  : var.cluster_size}"
  name           = "${element(var.instance_name, count.index)}-${count.index}"
  zone           = "${element(var.zones,count.index)}"
  network_id     = "${yandex_vpc_network.network.id}"
  v4_cidr_blocks = ["192.168.${count.index}.0/24"]
}

locals {
  subnet_ids = ["${yandex_vpc_subnet.subnet.*.id}"]
}

// Создайем целевую группу для lb_________________________________

resource "yandex_lb_target_group" "target-group" {
  name      = "target-group"

  target {
    subnet_id = "${local.subnet_ids.0[0]}"
    address   = "${yandex_compute_instance.instance.0.network_interface.0.ip_address}"
  }

  target {
    subnet_id = "${local.subnet_ids.0[1]}"
    address   = "${yandex_compute_instance.instance.1.network_interface.0.ip_address}"
  }

 #target {
 #   subnet_id = "${local.subnet_ids.0[2]}"
 #   address   = "${yandex_compute_instance.instance.2.network_interface.0.ip_address}"
 # }
 
}