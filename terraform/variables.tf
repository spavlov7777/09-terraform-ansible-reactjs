
variable "token" {
  description = "Yandex Cloud security OAuth token"
  #default = ""
}

variable "folder_id" {
  default = "b1gif2l7tpdc9gbksg1j"
}

variable "cloud_id" {
  default = "b1gtk6l52dh52mbok3d6"
}

variable "cores" {
  default ="2"
}

variable "memory" {
  default ="1"
}

variable "core_fraction" {
  default ="20"
}

variable "cluster_size" {
  default = 3
}

variable "zones" {
  description = "Yandex Cloud default Zone for provisioned resources"
  default = ["ru-central1-a","ru-central1-b","ru-central1-c"]
}

variable "instance_name" {
  default = ["nginx1","nginx2","app"]
}		