output "instance_external_ip_nginx1" {
  value = "${yandex_compute_instance.instance.0.network_interface.0.nat_ip_address}"
}

output "instance_internal_ip_nginx1" {
  value = "${yandex_compute_instance.instance.0.network_interface.0.ip_address}"
}
//---------------------------------------------------------------------------------------
output "instance_external_ip_nginx2" {
  value = "${yandex_compute_instance.instance.1.network_interface.0.nat_ip_address}"
}

output "instance_internal_ip_nginx2" {
  value = "${yandex_compute_instance.instance.1.network_interface.0.ip_address}"
}
//---------------------------------------------------------------------------------------
output "instance_external_ip_app" {
  value = "${yandex_compute_instance.instance.2.network_interface.0.nat_ip_address}"
}

output "instance_internal_ip_app" {
  value = "${yandex_compute_instance.instance.2.network_interface.0.ip_address}"
}