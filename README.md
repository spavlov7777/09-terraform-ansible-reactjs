# Порядок выполнения заданий
## Задание. Развернуть группу 3 Виртуальные машины и балансировщик и установить с помощю Ansible приложение ReactJS

Инициализация и запуск виртуальных машин
```bash
terraform init
terraform plan
terraform apply--auto-approve
```
Завершение работы виртуальных машин
```bash
terraform destroy --auto-approve
```
Далее переходим в папку Ansible и инициализируем запуск плейбука командой
```bash
ansible-playbook reactjs_playbook.yml -v
```
